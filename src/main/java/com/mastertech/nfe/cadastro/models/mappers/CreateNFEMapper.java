package com.mastertech.nfe.cadastro.models.mappers;

import com.mastertech.nfe.cadastro.models.NFE;
import com.mastertech.nfe.cadastro.models.dtos.CreateNFERequest;
import com.mastertech.nfe.cadastro.models.dtos.CreateNFEResponse;

public class CreateNFEMapper {

    public static CreateNFEResponse toCreateNFEResponse(NFE nfe) {
        CreateNFEResponse createNFEResponse = new CreateNFEResponse();
        createNFEResponse.setStatus(nfe.getStatus());
        return createNFEResponse;
    }

    public static NFE toNFE(CreateNFERequest createNFERequest) {
        NFE nfe = new NFE();
        nfe.setIdentidade(createNFERequest.getIdentidade());
        nfe.setValor(createNFERequest.getValor());
        return nfe;
    }
}
