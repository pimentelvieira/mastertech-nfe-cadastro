package com.mastertech.nfe.cadastro.models.dtos;

public class CreateNFEResponse {

    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
