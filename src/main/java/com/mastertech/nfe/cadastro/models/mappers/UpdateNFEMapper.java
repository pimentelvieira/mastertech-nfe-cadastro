package com.mastertech.nfe.cadastro.models.mappers;

import com.mastertech.nfe.cadastro.models.NFE;
import com.mastertech.nfe.cadastro.models.dtos.NFEDTO;

public class UpdateNFEMapper {

    public static NFE toNFE(NFEDTO nfeDTO, NFE nfe) {
        nfe.setIrrf(nfeDTO.getValorIRRF());
        nfe.setValorFinal(nfeDTO.getValorFinal());
        nfe.setCsll(nfeDTO.getValorCSLL());
        nfe.setCofins(nfeDTO.getValorCofins());
        return nfe;
    }
}
