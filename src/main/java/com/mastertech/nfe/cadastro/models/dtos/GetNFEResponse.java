package com.mastertech.nfe.cadastro.models.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GetNFEResponse {

    private String identidade;
    private Double valor;
    private String status;

    @JsonProperty("nfe")
    private NFEDTO nfeDTO;

    public String getIdentidade() {
        return identidade;
    }

    public void setIdentidade(String identidade) {
        this.identidade = identidade;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public NFEDTO getNfeDTO() {
        return nfeDTO;
    }

    public void setNfeDTO(NFEDTO nfeDTO) {
        this.nfeDTO = nfeDTO;
    }
}
