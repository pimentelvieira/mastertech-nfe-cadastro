package com.mastertech.nfe.cadastro.models.dtos;

public class CreateNFERequest {

    private String identidade;
    private Double valor;

    public String getIdentidade() {
        return identidade;
    }

    public void setIdentidade(String identidade) {
        this.identidade = identidade;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
