package com.mastertech.nfe.cadastro.models.mappers;

import com.mastertech.nfe.cadastro.models.NFE;
import com.mastertech.nfe.cadastro.models.dtos.GetNFEResponse;
import com.mastertech.nfe.cadastro.models.dtos.NFEDTO;

import java.util.List;
import java.util.stream.Collectors;

public class GetNFEMapper {

    public static GetNFEResponse toGetNFEResponse(NFE nfe) {
        GetNFEResponse getNFEResponse = new GetNFEResponse();
        getNFEResponse.setIdentidade(nfe.getIdentidade());
        getNFEResponse.setStatus(nfe.getStatus());
        getNFEResponse.setValor(nfe.getValor());

        if (!nfe.getStatus().equals("pending")) {
            NFEDTO nfeDTO = new NFEDTO();
            nfeDTO.setId(nfe.getId());
            nfeDTO.setValorCofins(nfe.getCofins());
            nfeDTO.setValorCSLL(nfe.getCsll());
            nfeDTO.setValorFinal(nfe.getValorFinal());
            nfeDTO.setValorInicial(nfe.getValor());
            nfeDTO.setValorIRRF(nfe.getIrrf());
            getNFEResponse.setNfeDTO(nfeDTO);
        }

        return getNFEResponse;
    }

    public static List<GetNFEResponse> toListGetNFEResponse(List<NFE> nfe) {
        return nfe.stream().map(n -> toGetNFEResponse(n)).collect(Collectors.toList());
    }
}
