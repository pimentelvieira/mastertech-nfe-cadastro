package com.mastertech.nfe.cadastro.repositories;

import com.mastertech.nfe.cadastro.models.NFE;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface NFERepository extends CrudRepository<NFE, Integer> {

    List<NFE> findAllByIdentidade(String identidade);
}
