package com.mastertech.nfe.cadastro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MastertechNfeCadastroApplication {

	public static void main(String[] args) {
		SpringApplication.run(MastertechNfeCadastroApplication.class, args);
	}

}
