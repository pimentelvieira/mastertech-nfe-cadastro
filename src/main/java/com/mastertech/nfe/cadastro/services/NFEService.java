package com.mastertech.nfe.cadastro.services;

import com.mastertech.nfe.cadastro.models.NFE;
import com.mastertech.nfe.cadastro.models.dtos.NFELogDTO;
import com.mastertech.nfe.cadastro.producers.NFELogProducer;
import com.mastertech.nfe.cadastro.producers.NFEProducer;
import com.mastertech.nfe.cadastro.repositories.NFERepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
public class NFEService {

    @Autowired
    private NFERepository repository;

    @Autowired
    private NFEProducer nfeProducer;

    @Autowired
    private NFELogProducer nfeLogProducer;

    public NFE createNFE(NFE nfe) {
        nfe.setStatus("pending");
        NFE nfeCreated = this.repository.save(nfe);
        nfeProducer.enviarAoKafka(nfeCreated);
        enviarMsgLog(nfe.getIdentidade(), nfe.getValor(), "Emissão");
        return nfeCreated;
    }

    public List<NFE> findAllByIdentidade(String identidade) {
        enviarMsgLog(identidade, null, "Consulta");
        return this.repository.findAllByIdentidade(identidade);
    }

    public NFE completeNFE(NFE nfe) {
        nfe.setStatus("complete");
        return this.repository.save(nfe);
    }

    public NFE findById(Integer id) {
        Optional<NFE> nfe = this.repository.findById(id);
        if (nfe.isPresent()) {
            return nfe.get();
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "NFE não encontrada");
        }
    }

    private void enviarMsgLog(String identidade, Double valor, String logType) {
        NFELogDTO nfeLogDTO = new NFELogDTO();
        nfeLogDTO.setLogType(logType);
        nfeLogDTO.setIdentidade(identidade);
        nfeLogDTO.setValor(valor);

        this.nfeLogProducer.enviarAoKafka(nfeLogDTO);
    }
}
