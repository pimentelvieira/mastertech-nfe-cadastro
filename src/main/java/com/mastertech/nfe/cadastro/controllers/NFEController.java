package com.mastertech.nfe.cadastro.controllers;

import com.mastertech.nfe.cadastro.models.NFE;
import com.mastertech.nfe.cadastro.models.dtos.CreateNFERequest;
import com.mastertech.nfe.cadastro.models.dtos.CreateNFEResponse;
import com.mastertech.nfe.cadastro.models.dtos.GetNFEResponse;
import com.mastertech.nfe.cadastro.models.dtos.NFEDTO;
import com.mastertech.nfe.cadastro.models.mappers.CreateNFEMapper;
import com.mastertech.nfe.cadastro.models.mappers.GetNFEMapper;
import com.mastertech.nfe.cadastro.models.mappers.UpdateNFEMapper;
import com.mastertech.nfe.cadastro.producers.NFEProducer;
import com.mastertech.nfe.cadastro.services.NFEService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class NFEController {

    @Autowired
    private NFEService service;

    @PostMapping("/nfe/emitir")
    @ResponseStatus(HttpStatus.CREATED)
    public CreateNFEResponse criarNFE(@RequestBody CreateNFERequest createNFERequest) {
        NFE nfe = this.service.createNFE(CreateNFEMapper.toNFE(createNFERequest));
        return CreateNFEMapper.toCreateNFEResponse(nfe);
    }

    @GetMapping("/nfe/consultar/{identidade}")
    public List<GetNFEResponse> findAllByIdentidade(@PathVariable String identidade) {
        return GetNFEMapper.toListGetNFEResponse(this.service.findAllByIdentidade(identidade));
    }

    @PutMapping("/nfe/{id}")
    public GetNFEResponse updateNFE(@RequestBody NFEDTO nfeDTO, @PathVariable Integer id) {
        NFE nfe = this.service.findById(id);
        return GetNFEMapper.toGetNFEResponse(this.service.completeNFE(UpdateNFEMapper.toNFE(nfeDTO, nfe)));
    }
}
