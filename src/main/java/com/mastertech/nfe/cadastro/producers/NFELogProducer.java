package com.mastertech.nfe.cadastro.producers;

import com.mastertech.nfe.cadastro.models.dtos.NFELogDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class NFELogProducer {

    @Autowired
    private KafkaTemplate<String, NFELogDTO> producer;

    public void enviarAoKafka(NFELogDTO nfeLogDTO) {
        producer.send("spec2-william-pimentel-2", nfeLogDTO);
    }
}
