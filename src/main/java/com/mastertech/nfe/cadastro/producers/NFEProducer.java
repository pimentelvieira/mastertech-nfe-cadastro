package com.mastertech.nfe.cadastro.producers;

import com.mastertech.nfe.cadastro.models.NFE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class NFEProducer {

    @Autowired
    private KafkaTemplate<String, NFE> producer;

    public void enviarAoKafka(NFE nfe) {
        producer.send("spec2-william-pimentel-1", nfe);
    }
}
